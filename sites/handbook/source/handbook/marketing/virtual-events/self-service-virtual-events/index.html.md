---
layout: handbook-page-toc
title: Self-Service Virtual Events
description: An overview of self-service virtual events and the process to run a virtual event as a GitLab team member.
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Self-Service Virtual Events Overview
{:.no_toc}
---

