---
layout: job_family_page
title: "Domestic Tax"
---

## Levels

### US Tax Manager (Intermediate)

The US Tax Manager (Intermediate) reports to Director, Tax Accounting and Domestic Tax.

#### US Tax Manager (Intermediate) Job Grade
The US Tax Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### US Tax Manager (Intermediate) Responsibilities

- Contribute to GitLab’s Global Tax Strategy
- Contribute to Global Tax Planning Strategies
- Contribute to USA Tax integration of acquisitions, dispositions and restructurings
- Contribute to the adherence to GitLab’s Transfer Pricing Model
- Ownership of US Federal, State and Local Tax Compliance
- Collaborate with our Tax Accountant on USA Sales & Use Tax Compliance
- Contribute to the mitigation of Permanent Establishment exposure
- Contribute to the mitigation of Employment Tax exposure
- Contribute to US Tax Audits and participate in developing strategies
- Contribute to the US and Foreign Tax Provisioning
- Contribute to the maintenance of GitLab’s Global Corporate Structure
- Evaluate and perform calculations for uncertain tax position accruals (e.g. FIN48, FAS5)
- Analyze the tax aspects of GitLab’s Financial Model (e.g. BEAT, GILTI, FDII)
- Collaborate with the Finance Organization (Accounting, Treasury Reporting & Compliance)
- Collaborate with the Legal Organization (e.g. corporate structure, registrations)
- Collaborate with Global Sales Team to Withholding Tax Related Questions
- Collaborate with Payroll & Stock Administrator on US tax implications of stock options 
- Contribute to technology improvements to ensure fast and accurate data processing
- Implementation and assurance of control cycle compliant with SOX
- Collaborate with our IT department for tax settings of Finance IT Systems (e.g. ERP)
- Increase efficiency for existing processes and design new processes (e.g. use of AI)
- Contribute to GitLab’s international expansion activities (e.g. entity setup)
- Collaborate with GitLab Team Members on day-to-day activities, requests and projects
- Contribute to the overall finance & tax OKR’s
- Exposure to C-suite for larger global projects (e.g. Tax Strategy)
- This position reports to the Director of Tax

#### US Tax Manager (Intermediate) Requirements

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* 5-8 years experience in a Big 4 environment and/or Business environment
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* Working knowledge of multiple International tax areas
* Ability to managing multiple projects at the same time
* Ability to use GitLab

#### Senior US Tax Manager

The Senior US Tax Manager reports to Director, Tax Accounting and Domestic Tax.

#### Senior US Tax Manager Job Grade

The Senior US Tax Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior US Tax Manager Responsibilities

* Extends that of the US Tax Manager (Intermediate) responsibilities

#### Senior US Tax Manager Requirements

* Exends that of the US Tax Manager (Intermediate) requirements
- Degree in Business Taxation (Master's / JD / CPA)
- Proven track record of personal growth in your career path
- 8-12 years experience in a Big 4 environment and Business environment
- Successfully leading large projects and/or change management projects
- Strong influencing skills, high adaptability and analytical thinking
- Expert in US Tax and International tax 
- Leading multiple projects at the same time

### Staff US Tax Manager

The Staff US Tax Manager reports to Director, Tax Accounting and Domestic Tax.

#### Staff US Tax Manager Job Grade

The Staff US Tax Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff US Tax Manager Responsibilities

* Extends that of the Senior US Tax Manager responsibilities

#### Staff US Tax Manager Requirements

* Extends that of the Senior US Tax Manager requirements
* Degree in Business Taxation (Master's / JD / CPA)
* 15+ years experience in a Big 4 environment and Business environment
* Strong business judgment applied to tax and finance operational activities
* Proven international experience working with overseas operations
* Proven success with managing tax on a global scale within a fast pace environment
* Proven ability to drive change in tax processes with a technology driven mindset
* Demonstrated experience in mentoring and leading a distributed team
* Leadership experience in a software or global technology company
* Proven strategic and tactical vision to lead a high performing team

### Performance Indicators

* [Effective Tax Rate](https://about.gitlab.com/handbook/tax/performance-indicators/#effective-tax-rate-etr)
* [Budget vs. Actual](https://about.gitlab.com/handbook/tax/performance-indicators/#budget-vs-actual)
* [Audit Adjustments](https://about.gitlab.com/handbook/tax/performance-indicators/#audit-adjustments)
* [International Expansion](https://about.gitlab.com/handbook/tax/performance-indicators/#international-expansion)

### Director, Tax Accounting and Domestic Tax

The Director, Tax Accounting and Domestic Tax report to the VP Tax and will interact with one peer and several Tax Department members.

#### Director, Tax Accounting and Domestic Tax Job Grade

The Director of Tax Accounting and Domestic Tax is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Tax Accounting and Domestic Tax Responsibilities

* Staff mentoring and supervision
* SOX control implementation, testing, and narrative writing
* Process implementation for global income tax accounting
* Interaction with Big 4 financial statement auditors
* Working with outside service providers on projects including but not limited to:
* Domestic income tax compliance
* Sales tax compliance
* Maintenance of a domestic compliance calendar
* Domestic tax audit supervision, both direct and indirect taxes
* M&A and purchase accounting (income and indirect taxes)
* Tax account reconciliation
* Working with Finance team members on intercompany settlements

#### Director, Tax Accounting and Domestic Tax Requirements

* Bachelor’s Degree (B.S.) in Accounting. Master’s Degree in Business Taxation preferred.
* JD and/or CPA preferred.
* Experience with Software and/or SAAS in a high growth environment.
* Technical understanding of ASC 740, ASC 718, and ASC 805 for global operations
* Spreadsheet modeling for ASC 740, tax return workpapers, and project planning
* Tax and tax provision issues relating to stock based compensation, including contra-DTAs produced by Sec. 162M
* Some working knowledge of VAT/GST, transfer pricing, and US international taxes including FDII, GILTI, BEAT, FTCs and Subpart F
* Project management
* Audit supervision
* Memo writing
* Taking initiative and also working as a team player
* Experience with NetSuite, SalesForce, Zuora, Coupa, GSuite and Avalara are plusses.
* Ability to use GitLab

## Performance Indicators

* Timely, complete, and accurate completion of tax accounting
* Timely, complete, and accurate completion of tax returns
* Efficient use of company resources
* Project management (successful drive projects to completion, regardless of the resource chosen)
* Technical abilities

## Career Ladder

The next step in the Domestic Tax job family is not yet defined.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.

* Next, candidates will be invited to schedule a first interview with our VP, Tax
* Candidates will then be invited to schedule an interview with our Director, Tax
* Next, candidates will be invited to schedule an interview with our VP, Corporate Controller
* Finally, candidates will have a 30min call with either our CFO

Additional details about our process can be found on our [hiring page](/handbook/hiring/)
